package com.projet.foodtruck.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "desserts")
public class Dessert {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;

    public Dessert(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Dessert{" +
                "id=" + id +
                ", nom='" + name + '\'' +
                "}";
    }
}
