package com.projet.foodtruck.DAO;

import androidx.room.TypeConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListTypeConverter {

    @TypeConverter
    public List<String> stringToList(String value){
        String[] strArr = value.split("\\s+");
        List<String> ingredients = new ArrayList<>(Arrays.asList(strArr));
        return ingredients;
    }

    @TypeConverter
    public String listToString(List<String> ingredients){
        String res = "";
        for(String ingr : ingredients){
            res+=ingr+" ";
        }
        return res;
    }
}
