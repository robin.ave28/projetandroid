package com.projet.foodtruck.models;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.projet.foodtruck.DAO.FoodTruckRepository;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class FoodTruckViewModel extends AndroidViewModel {

    private FoodTruckRepository foodTruckRepository;
    private LiveData<List<SandwichWithIngredients>> lesSandwichs;
    private LiveData<Integer> nbSandwichs;

    private LiveData<List<IngredientWithSandwichs>> lesIngredients;
    private LiveData<Integer> nbIngredients;

    private LiveData<List<Sandwich>> sandwichsOfIngredient;



    public FoodTruckViewModel(Application app){
        super(app);
        foodTruckRepository = new FoodTruckRepository(app);
        nbSandwichs=foodTruckRepository.getNbSandwichs();
        lesSandwichs=foodTruckRepository.getLesSandwichs();

        nbIngredients=foodTruckRepository.getNbIngredients();
        lesIngredients=foodTruckRepository.getLesIngredients();
    }

    public LiveData<Integer> getNbSandwichs(){
        return nbSandwichs;
    }


    public LiveData<List<SandwichWithIngredients>> getLesSandwichs() {
        return lesSandwichs;
    }

    public LiveData<Integer> getNbIngredients(){
        return nbIngredients;
    }

    public Ingredient getIngredient(Integer id) throws ExecutionException, InterruptedException {
        return foodTruckRepository.getIngredient(id);
    }

    public String getIngredientName(String name) throws ExecutionException, InterruptedException {
        return foodTruckRepository.getIngredientName(name);
    }

    public Sandwich getSandwich(Integer id) throws ExecutionException, InterruptedException {
        return foodTruckRepository.getSandwich(id);
    }

    public String getSandwichName(String name) throws ExecutionException, InterruptedException {
        return foodTruckRepository.getSandwichName(name);
    }

    public LiveData<List<Sandwich>> getSandwichsOfIngredient(Integer id){
        return foodTruckRepository.getSandwichsOfIngredient(id);
    }

    public SandwichWithIngredients getSandwichWithIngredients(Integer id) throws ExecutionException, InterruptedException {
        return foodTruckRepository.getSandwichWithIngredients(id);
    }

    public IngredientWithSandwichs getIngredientWithSandwichs(Integer id) throws ExecutionException, InterruptedException {
        return foodTruckRepository.getIngredientWithSandwichs(id);
    }


    public LiveData<List<IngredientWithSandwichs>> getLesIngredients() {
        return lesIngredients;
    }

//    public LiveData<Sandwich> getSandwich(Integer id){
//        return foodTruckRepository.getSandwich(id);
//    }

    public void deleteSandwichs(){
        foodTruckRepository.deleteSandwichs();
    }

    public void deleteSandwich(Integer id){
        foodTruckRepository.deleteSandwich(id);
    }

    public void deleteIngredient(Integer id){
        foodTruckRepository.deleteIngredient(id);
    }


    public void deleteSandwichsIngredientCrossRef(){
        foodTruckRepository.deleteSandwichsIngredientCrossRef();}

    public void deleteSandwichsIngredientCrossRefById(Integer id){
        foodTruckRepository.deleteSandwichsIngredientCrossRefById(id);}

    public void deleteSandwichsIngredientCrossRefByIdIngredient(Integer id){
        foodTruckRepository.deleteSandwichsIngredientCrossRefByIdIngredient(id);}

    public void insert(Sandwich s){
        foodTruckRepository.insertSandwich(s);
    }

    public long insertSandwich(Sandwich s){
        return foodTruckRepository.insertSandwich(s);
    }

    public long insertIngredient(Ingredient ing){
        return foodTruckRepository.insertIngredient(ing);
    }

    public void deleteIngredients(){
        foodTruckRepository.deleteIngredients();
    }



    public void insertSandwichWithIngredients(Sandwich sand, Ingredient ingr){
        foodTruckRepository.insertSandwichWithIngredients(sand, ingr);
    }

    public void insertIngredientWithSandwichs(Sandwich sand, Ingredient ingr){
        foodTruckRepository.insertIngredientWithSandwichs(sand, ingr);
    }

}
