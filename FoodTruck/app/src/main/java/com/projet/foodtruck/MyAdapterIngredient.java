package com.projet.foodtruck;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.projet.foodtruck.models.Ingredient;
import com.projet.foodtruck.models.Sandwich;
import com.projet.foodtruck.tools.InterfaceMyListener;
import com.projet.foodtruck.tools.MyAdapter;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class MyAdapterIngredient extends RecyclerView.Adapter<MyAdapterIngredient.MyViewHolder>{

    private List<Ingredient> lesIngredients;
    private static InterfaceMyListener myListener;



    public MyAdapterIngredient(List<Ingredient> ingredients){ this.lesIngredients=ingredients;}


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener{

        private TextView tvNom;
        private Ingredient ingredient;

        public MyViewHolder(@NonNull View itemView){
            super(itemView);
            tvNom=itemView.findViewById(R.id.nomIngredient);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);


        }

        public void display(Ingredient ingredient){
            tvNom.setText(ingredient.getName());



            this.ingredient = ingredient;
        }

        @Override
        public void onClick(View view) {
            try {
                myListener.onItemClick(getAdapterPosition(), view);
            } catch (ExecutionException e) {
                System.out.println("C'EST LA MERDE");
                e.printStackTrace();
            } catch (InterruptedException e) {
                System.out.println("C'EST LA MERDE");
                e.printStackTrace();
            }

        }

        @Override
        public boolean onLongClick(View view) {
            try {
                myListener.onItemLongClick(getAdapterPosition(), view);
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return false;
        }
    }


    @NonNull
    @Override
    public MyAdapterIngredient.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ingredient,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapterIngredient.MyViewHolder holder, int position) {
        Ingredient ingredient = lesIngredients.get(position);
        holder.display(ingredient);
    }

    @Override
    public int getItemCount() {
        return lesIngredients.size();
    }

    public static void setMyListener(InterfaceMyListener myListener) {
        MyAdapterIngredient.myListener = myListener;
    }
}
