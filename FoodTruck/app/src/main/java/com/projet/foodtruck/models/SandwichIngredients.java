package com.projet.foodtruck.models;

import androidx.room.Entity;


@Entity(primaryKeys={"sandwichId","ingredientId"})
public class SandwichIngredients {
    int sandwichId;
    int ingredientId;

    public SandwichIngredients(int sandwichId, int ingredientId){
        this.sandwichId=sandwichId;
        this.ingredientId=ingredientId;
    }

    public int getSandwichId() {
        return sandwichId;
    }

    public int getIngredientId() {
        return ingredientId;
    }
}
