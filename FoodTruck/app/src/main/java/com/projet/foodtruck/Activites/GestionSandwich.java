package com.projet.foodtruck.Activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projet.foodtruck.R;
import com.projet.foodtruck.models.FoodTruckViewModel;
import com.projet.foodtruck.models.Ingredient;
import com.projet.foodtruck.models.Sandwich;
import com.projet.foodtruck.models.SandwichWithIngredients;
import com.projet.foodtruck.tools.InterfaceMyListener;
import com.projet.foodtruck.tools.MyAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class GestionSandwich extends AppCompatActivity {

    private FoodTruckViewModel foodTruckViewModel;
    private List<SandwichWithIngredients> lesSandwichs = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MyAdapter myAdapter;

//    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
//            new ActivityResultCallback<ActivityResult>() {
//                @Override
//                public void onActivityResult(ActivityResult result) {
//
//                    if (result.getResultCode() == Activity.RESULT_OK) {
//                        System.out.println("YOYOOYOYOYOYOYOOYOYOYOYO");
//
//                        Intent intent = result.getData();
//                        SandwichWithIngredients s = intent.getParcelableExtra("sandwich");
//                        lesSandwichs.add(s);
//                        //foodTruckViewModel.insert(s);
//                        myAdapter.notifyItemChanged(lesSandwichs.size()-1);
//
//                    }
//                }
//            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gestion_sandwich);


        foodTruckViewModel = new ViewModelProvider(this).get(FoodTruckViewModel.class);

        foodTruckViewModel.getNbSandwichs().observe(this, new Observer<Integer>() {

            @Override
            public void onChanged(Integer integer) {

                System.out.println("Changement");
                TextView tv = findViewById(R.id.nbSandwichs);
                tv.setText("Nb sandwichs: "+integer );
            }
        });


        RecyclerView recyclerView = findViewById(R.id.recyclerSandwich);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        myAdapter = new MyAdapter(lesSandwichs);
        recyclerView.setAdapter(myAdapter);

        try {

            Bundle bundle = getIntent().getExtras();
            Sandwich s = (Sandwich) bundle.get("sandwich");
            SandwichWithIngredients sand = foodTruckViewModel.getSandwichWithIngredients(s.getSandwichId());
            lesSandwichs.add(sand);

            myAdapter.notifyItemChanged(lesSandwichs.size()-1);

        }catch (NullPointerException nullPointerException){
            System.out.println("Bundle non existant à la première arrivée");
            nullPointerException.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        foodTruckViewModel.getLesSandwichs().observe(this, new Observer<List<SandwichWithIngredients>>() {
            @Override
            public void onChanged(List<SandwichWithIngredients> sandwichs) {
                lesSandwichs.clear();
                System.out.println("ON RENTRE DANS ONCHANGED");
                System.out.println(sandwichs);
                for(SandwichWithIngredients s : sandwichs){
                    lesSandwichs.add(s);

                }
                myAdapter.notifyDataSetChanged(); 
                System.out.println(lesSandwichs);
            }
        });



        MyAdapter.setMyListener(new InterfaceMyListener() {
            @Override
            public void onItemLongClick(int position, View view)  {
                Integer id = lesSandwichs.get(position).sandwich.getSandwichId();
                lesSandwichs.remove(position);
                foodTruckViewModel.deleteSandwich(id);
                foodTruckViewModel.deleteSandwichsIngredientCrossRefById(id);
                myAdapter.notifyItemRemoved(position);
                //Integer id = lesSandwichs.get(position).sandwich.getSandwichId();
                //lesSandwichs.remove(position);
                //foodTruckViewModel.deleteSandwich(id);
                //myAdapter.notifyItemRemoved(position);
                System.out.println("ALLLEDDD");
            }

            @Override
            public void onItemClick(int position, View view) {


            }
        });

    }


    public void generer(View view) {
        for(int i=0;i<10;i++){
            System.out.println("Ajout sandwich");
            Sandwich sand = new Sandwich("Sandwich "+i, "1€", "categ_sandwich");
            foodTruckViewModel.insert(sand);
        }

    }

    public void supprimer(View view) {
        foodTruckViewModel.deleteSandwichsIngredientCrossRef();
        foodTruckViewModel.deleteSandwichs();

        lesSandwichs.clear();
        myAdapter.notifyDataSetChanged();
    }

    public void allerGestion(View view) {
        Intent intent = new Intent(getApplicationContext(), AccueilGestion.class);
        startActivity(intent);
        finish();
    }

    public void ajoutSandwich(View view) {
        Intent intent = new Intent(this,AjoutSandwich.class);
        //mStartForResult.launch(intent);
        startActivity(intent);
        finish();
    }
}
