package com.projet.foodtruck.Activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.projet.foodtruck.DAO.ListTypeConverter;
import com.projet.foodtruck.R;
import com.projet.foodtruck.models.Sandwich;

public class ModifSandwich extends AppCompatActivity {
    ListTypeConverter listTypeConverter;

    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent intent = result.getData();
                        Sandwich s = intent.getParcelableExtra("id");


                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modif_sandwich);

        Sandwich s = (Sandwich) getIntent().getParcelableExtra("sandwich");
        EditText et = findViewById(R.id.editSandwichName);
        et.setText(s.getName());
        EditText et2 = findViewById(R.id.editSandwichPrice);
        et2.setText(s.getPrice());


    }
}
