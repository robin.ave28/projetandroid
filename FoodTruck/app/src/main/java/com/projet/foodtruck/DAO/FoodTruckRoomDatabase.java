package com.projet.foodtruck.DAO;

import  android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.projet.foodtruck.models.Boisson;
import com.projet.foodtruck.models.Dessert;
import com.projet.foodtruck.models.Ingredient;
import com.projet.foodtruck.models.Salade;
import com.projet.foodtruck.models.Sandwich;
import com.projet.foodtruck.models.SandwichIngredients;

@Database(entities = {Salade.class, Sandwich.class, Boisson.class, Dessert.class, Ingredient.class, SandwichIngredients.class}, version=1)
@TypeConverters({ListTypeConverter.class})
public abstract class FoodTruckRoomDatabase extends RoomDatabase {
    public abstract FoodTruckDAO foodTruckDAO();

    public static FoodTruckRoomDatabase INSTANCE;

    public static FoodTruckRoomDatabase getDataBase(final Context context){
        if(INSTANCE==null){
            synchronized (FoodTruckRoomDatabase.class){
                if(INSTANCE==null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            FoodTruckRoomDatabase.class, "FoodTruckDatabase").build();
                }
            }
        }
        return INSTANCE;
    }
}
