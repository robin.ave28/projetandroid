package com.projet.foodtruck.models;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class IngredientWithSandwichs {
    @Embedded public Ingredient ingredient;
    @Relation(
            parentColumn = "ingredientId",
            entityColumn = "sandwichId",
            associateBy = @Junction(SandwichIngredients.class)
    )
    public List<Sandwich> sandwichs;
}
