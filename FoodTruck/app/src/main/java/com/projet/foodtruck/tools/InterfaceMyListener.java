package com.projet.foodtruck.tools;

import android.view.View;

import java.util.concurrent.ExecutionException;

public interface InterfaceMyListener {

    void onItemLongClick(int position, View view) throws  ExecutionException, InterruptedException;

    void onItemClick(int position, View view) throws ExecutionException, InterruptedException;
}
