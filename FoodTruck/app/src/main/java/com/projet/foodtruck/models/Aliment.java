package com.projet.foodtruck.models;

import java.util.ArrayList;
import java.util.List;

public class Aliment {

    String nom;
    String type;
    List<String> ingredients = new ArrayList<>();

    public Aliment(String nom, String type, List<String> ingredients){
        this.nom=nom;
        this.type=type;
        this.ingredients=ingredients;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getIngredients(){
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }
}
