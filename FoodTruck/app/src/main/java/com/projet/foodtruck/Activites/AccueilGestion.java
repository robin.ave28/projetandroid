package com.projet.foodtruck.Activites;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.projet.foodtruck.R;

public class AccueilGestion extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accueil_gestion);

    }

    public void allerAccueil(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void allerGestionSandwich(View view) {
        Intent intent = new Intent(getApplicationContext(), GestionSandwich.class);
        startActivity(intent);
        finish();
    }

    public void allerGestionSalade(View view) {
    }

    public void allerGestionBoisson(View view) {
    }

    public void allerGestionDessert(View view) {
    }

    public void allerGestionIngredients(View view) {
        Intent intent = new Intent(getApplicationContext(), GestionIngredient.class);
        startActivity(intent);
        finish();
    }
}
