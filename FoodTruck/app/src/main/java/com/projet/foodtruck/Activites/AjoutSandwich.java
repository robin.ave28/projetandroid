package com.projet.foodtruck.Activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projet.foodtruck.DAO.ListTypeConverter;
import com.projet.foodtruck.MyAdapterIngredient;
import com.projet.foodtruck.R;
import com.projet.foodtruck.models.FoodTruckViewModel;
import com.projet.foodtruck.models.Ingredient;
import com.projet.foodtruck.models.Sandwich;
import com.projet.foodtruck.models.SandwichWithIngredients;
import com.projet.foodtruck.tools.MyAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class AjoutSandwich extends AppCompatActivity {

    ListTypeConverter converter = new ListTypeConverter();

    private FoodTruckViewModel foodTruckViewModel;
    private List<Ingredient> lesIngredients = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MyAdapterIngredient myAdapter;

//    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
//            new ActivityResultCallback<ActivityResult>() {
//                @Override
//                public void onActivityResult(ActivityResult result) {
//                    System.out.println("arsouze");
//                    if (result.getResultCode() == Activity.RESULT_OK) {
//                        System.out.println("JEIOEAJRPOEZFOPEGFKEGIOEJEJGOJK");
//                        Intent intent = result.getData();
//                        Integer id_ingredient = intent.getParcelableExtra("id");
//
//
//                            try {
//                                lesIngredients.add(foodTruckViewModel.getIngredient(id_ingredient));
//                                System.out.println(id_ingredient);
//                            } catch (ExecutionException e) {
//                                e.printStackTrace();
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//
//
//                        myAdapter.notifyDataSetChanged();
//
//                    }
//
//                    try {
//
//                        Bundle bundle = getIntent().getExtras();
//                        List<Ingredient> ingredientsSelect = (List<Ingredient>)  bundle.get("ingredients");
//                        for(Ingredient i:ingredientsSelect){
//                            lesIngredients.add(i);
//                        }
//                        System.out.println(lesIngredients);
//                        myAdapter.notifyDataSetChanged();
//
//                    }catch (NullPointerException nullPointerException){
//                        nullPointerException.printStackTrace();
//                    }
//                }
//            });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajout_sandwich);

        foodTruckViewModel = new ViewModelProvider(this).get(FoodTruckViewModel.class);

        RecyclerView recyclerView = findViewById(R.id.recyclerIngredientSand);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        myAdapter = new MyAdapterIngredient(lesIngredients);
        recyclerView.setAdapter(myAdapter);

        try {

            Bundle bundle = getIntent().getExtras();
            List<Ingredient> ingredientsSelect = (List<Ingredient>) bundle.get("ingredients");

            for(Ingredient i:ingredientsSelect){
                lesIngredients.add(i);
            }
            System.out.println(lesIngredients);
            myAdapter.notifyDataSetChanged();
            //lesIngredients.add(ingredient);
            //System.out.println(ingredient.getName());
            //myAdapter.notifyDataSetChanged();

        }catch (NullPointerException nullPointerException){
            System.out.println("Bundle non existant à la première arrivée");
            nullPointerException.printStackTrace();
        }


    }


    @Override
    public void finish() {
        super.finish();
    }



    public void backToGestionSand(View view) {
        Intent intent = new Intent(this,GestionSandwich.class);
        //mStartForResult.launch(intent);
        startActivity(intent);
        finish();
    }

    public void addSandwich(View view) throws ExecutionException, InterruptedException {

        TextView tvName = findViewById(R.id.editSandwich);
        String nom = tvName.getText().toString();

        TextView tvPrice = findViewById(R.id.editPrice);
        String price = tvPrice.getText().toString();

        Intent intent = new Intent(this, GestionSandwich.class);

        if(foodTruckViewModel.getSandwichName(nom)==null){
            Sandwich c = new Sandwich(nom , price, "categ_sandwich");

            long idSand = foodTruckViewModel.insertSandwich(c);
            int idSandwich = (int) idSand;
            System.out.println("ID LAAA");
            System.out.println(idSandwich);
            c = foodTruckViewModel.getSandwich(idSandwich);

            for(Ingredient i:lesIngredients){
                System.out.println("ID INGREDIENT LAA");
                System.out.println(i.getIngredientId());
                foodTruckViewModel.insertSandwichWithIngredients(c, i);
                foodTruckViewModel.insertIngredientWithSandwichs(c, i);
            }
            SandwichWithIngredients sandWithIngr = foodTruckViewModel.getSandwichWithIngredients(idSandwich);
            intent.putExtra("sandwich", c);

        }else{
            Toast toast = Toast.makeText(getApplicationContext(), "Le sandwich existe déjà !", 2);
            toast.show();
        }


        //setResult(Activity.RESULT_OK, intent);
        startActivity(intent);
        finish();
    }

    public void addIngredientSand(View view) {
        Intent intent = new Intent(this,AjoutIngredientSandwich.class);
        startActivity(intent);
        finish();
    }

//    public void addIngredientSand(View view) {
//        Intent intent = new Intent(this,AjoutIngredientSandwich.class);
//        mStartForResult.launch(intent);
//    }
}
