package com.projet.foodtruck.DAO;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.net.ContentHandler;

public abstract class SaladeRoomDatabase extends RoomDatabase {
    public abstract SaladeDAO saladeDAO();

    public static SaladeRoomDatabase INSTANCE;

    public static SaladeRoomDatabase getDataBase(final Context context){
        if(INSTANCE==null){
            synchronized (SaladeRoomDatabase.class){
                if(INSTANCE==null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            SaladeRoomDatabase.class, "Salades_database").build();
                }
            }
        }
        return INSTANCE;
    }
}
