package com.projet.foodtruck.DAO;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.room.Query;

import com.projet.foodtruck.models.Boisson;
import com.projet.foodtruck.models.Dessert;
import com.projet.foodtruck.models.Ingredient;
import com.projet.foodtruck.models.IngredientWithSandwichs;
import com.projet.foodtruck.models.Salade;
import com.projet.foodtruck.models.Sandwich;
import com.projet.foodtruck.models.SandwichIngredients;
import com.projet.foodtruck.models.SandwichWithIngredients;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class FoodTruckRepository {
    private FoodTruckDAO foodTruckDAO;
    private LiveData<List<Salade>> lesSalades;
    private LiveData<List<SandwichWithIngredients>> lesSandwichs;
    private LiveData<Integer> nbSandwichs;
    private LiveData<Sandwich> sandwich;
    private LiveData<List<Sandwich>> sandwichsOfIngredient;
    private int nbSand;
    private LiveData<List<Boisson>> lesBoissons;
    private LiveData<List<Dessert>> lesDesserts;

    private LiveData<List<IngredientWithSandwichs>> lesIngredients;
    private LiveData<Integer> nbIngredients;

    public FoodTruckRepository(Application application){
        FoodTruckRoomDatabase db = FoodTruckRoomDatabase.getDataBase(application);
        foodTruckDAO = db.foodTruckDAO();
        lesSalades=foodTruckDAO.toutesLesSalades();

        lesSandwichs=foodTruckDAO.tousLesSandwichs();
        nbSandwichs=foodTruckDAO.nbSandwichs();

        lesIngredients=foodTruckDAO.tousLesIngredients();
        nbIngredients=foodTruckDAO.nbIngredients();

        lesBoissons=foodTruckDAO.toutesLesBoissons();
        lesDesserts=foodTruckDAO.tousLesDesserts();
    }

    public LiveData<Integer> getNbSandwichs() {
        return nbSandwichs;
    }

    public LiveData<List<SandwichWithIngredients>> getLesSandwichs() {
        return lesSandwichs;
    }

    public LiveData<Integer> getNbIngredients() {
        return nbIngredients;
    }

    public LiveData<List<IngredientWithSandwichs>> getLesIngredients() {
        return lesIngredients;
    }

//    public LiveData<Sandwich> getSandwich(Integer id){
//        return foodTruckDAO.getSandwich(id);
//
//    }

    public Ingredient getIngredient(Integer id) throws ExecutionException, InterruptedException{
        return new getAsyncTaskIngredient(foodTruckDAO, id).execute().get();
    }

    private static class getAsyncTaskIngredient extends AsyncTask<Void, Void, Ingredient>{
        private FoodTruckDAO foodTruckDAO;
        private int id;

        getAsyncTaskIngredient(FoodTruckDAO fd, int id){
            foodTruckDAO=fd;
            this.id=id;
        }

        @Override
        protected Ingredient doInBackground(Void... voids) {
            return foodTruckDAO.getIngredient(id);
        }
    }

    public String getIngredientName(String name) throws ExecutionException, InterruptedException{
        return new getAsyncTaskIngredientName(foodTruckDAO, name).execute().get();
    }

    private static class getAsyncTaskIngredientName extends AsyncTask<Void, Void, String>{
        private FoodTruckDAO foodTruckDAO;
        private String name;

        getAsyncTaskIngredientName(FoodTruckDAO fd, String name){
            foodTruckDAO=fd;
            this.name=name;
        }

        @Override
        protected String doInBackground(Void... voids) {
            return foodTruckDAO.getIngredientName(name);
        }
    }

    public Sandwich getSandwich(Integer id) throws ExecutionException, InterruptedException{
        return new getAsyncTaskSandwich(foodTruckDAO, id).execute().get();

    }

    public LiveData<List<Sandwich>> getSandwichsOfIngredient(Integer id){
        return foodTruckDAO.getSandwichsOfIngredient(id);
    }

    private static class getAsyncTaskSandwich extends AsyncTask<Void, Void, Sandwich>{
        private FoodTruckDAO foodTruckDAO;
        private int id;

        getAsyncTaskSandwich(FoodTruckDAO fd, int id){
            foodTruckDAO=fd;
            this.id=id;
        }

        @Override
        protected Sandwich doInBackground(Void... voids) {
            return foodTruckDAO.getSandwich(id);
        }
    }

    public String getSandwichName(String name) throws ExecutionException, InterruptedException{
        return new getAsyncTaskSandwichName(foodTruckDAO, name).execute().get();
    }

    private static class getAsyncTaskSandwichName extends AsyncTask<Void, Void, String>{
        private FoodTruckDAO foodTruckDAO;
        private String name;

        getAsyncTaskSandwichName(FoodTruckDAO fd, String name){
            foodTruckDAO=fd;
            this.name=name;
        }

        @Override
        protected String doInBackground(Void... voids) {
            return foodTruckDAO.getSandwichName(name);
        }
    }

    //GET SANDWICH WITH INGREDIENTS
    public SandwichWithIngredients getSandwichWithIngredients(Integer id) throws ExecutionException, InterruptedException{
        return new getAsyncTaskSandwichWithIngredients(foodTruckDAO, id).execute().get();

    }

    private static class getAsyncTaskSandwichWithIngredients extends AsyncTask<Void, Void, SandwichWithIngredients>{
        private FoodTruckDAO foodTruckDAO;
        private int id;

        getAsyncTaskSandwichWithIngredients(FoodTruckDAO fd, int id){
            foodTruckDAO=fd;
            this.id=id;
        }

        @Override
        protected SandwichWithIngredients doInBackground(Void... voids) {
            return foodTruckDAO.getSandwichWithIngredients(id);
        }
    }

    //GET INGREDIENT WITH SANDWICHS
    public IngredientWithSandwichs getIngredientWithSandwichs(Integer id) throws ExecutionException, InterruptedException{
        return new getAsyncTaskIngredientWithSandwichs(foodTruckDAO, id).execute().get();

    }

    private static class getAsyncTaskIngredientWithSandwichs extends AsyncTask<Void, Void, IngredientWithSandwichs>{
        private FoodTruckDAO foodTruckDAO;
        private int id;

        getAsyncTaskIngredientWithSandwichs(FoodTruckDAO fd, int id){
            foodTruckDAO=fd;
            this.id=id;
        }

        @Override
        protected IngredientWithSandwichs doInBackground(Void... voids) {
            return foodTruckDAO.getIngredientWithSandwichs(id);
        }
    }
    //========\\

    public void deleteSandwichs(){
        new deleteAsyncTask(foodTruckDAO).execute();
    }

    private static class deleteAsyncTask extends AsyncTask<Void, Void, Void>{
        private FoodTruckDAO foodTruckDAO;

        deleteAsyncTask(FoodTruckDAO fd){
            foodTruckDAO=fd;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            foodTruckDAO.deleteSandwichs();
            return null;
        }
    }

    public void deleteSandwich(Integer id){
        new deleteAsyncTaskSandwich(foodTruckDAO, id).execute();
    }

    private static class deleteAsyncTaskSandwich extends AsyncTask<Void, Void, Void>{
        private FoodTruckDAO foodTruckDAO;
        private Integer id;

        deleteAsyncTaskSandwich(FoodTruckDAO fd, Integer id){
            foodTruckDAO=fd;
            this.id=id;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            foodTruckDAO.deleteSandwich(id);
            return null;
        }
    }

    public void deleteIngredient(Integer id){
        new deleteAsyncTaskIngredient(foodTruckDAO, id).execute();
    }

    private static class deleteAsyncTaskIngredient extends AsyncTask<Void, Void, Void>{
        private FoodTruckDAO foodTruckDAO;
        private Integer id;

        deleteAsyncTaskIngredient(FoodTruckDAO fd, Integer id){
            foodTruckDAO=fd;
            this.id=id;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            foodTruckDAO.deleteIngredient(id);
            return null;
        }
    }

    public void deleteSandwichsIngredientCrossRef(){
        new deleteAsyncTaskCrossRef(foodTruckDAO).execute();
    }

    private static class deleteAsyncTaskCrossRef extends AsyncTask<Void, Void, Void>{
        private FoodTruckDAO foodTruckDAO;

        deleteAsyncTaskCrossRef(FoodTruckDAO fd){
            foodTruckDAO=fd;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            foodTruckDAO.deleteSandwichIngredientCrossRef();
            return null;
        }
    }

    public void deleteSandwichsIngredientCrossRefById(Integer id){
        new deleteAsyncTaskCrossRefById(foodTruckDAO, id).execute();
    }

    private static class deleteAsyncTaskCrossRefById extends AsyncTask<Void, Void, Void>{
        private FoodTruckDAO foodTruckDAO;
        private Integer id;

        deleteAsyncTaskCrossRefById(FoodTruckDAO fd, Integer id){
            foodTruckDAO=fd;
            this.id=id;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            foodTruckDAO.deleteSandwichIngredientCrossRefById(id);
            return null;
        }
    }

    public void deleteSandwichsIngredientCrossRefByIdIngredient(Integer id){
        new deleteAsyncTaskCrossRefById(foodTruckDAO, id).execute();
    }

    private static class deleteAsyncTaskCrossRefByIdIngredient extends AsyncTask<Void, Void, Void>{
        private FoodTruckDAO foodTruckDAO;
        private Integer id;

        deleteAsyncTaskCrossRefByIdIngredient(FoodTruckDAO fd, Integer id){
            foodTruckDAO=fd;
            this.id=id;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            foodTruckDAO.deleteSandwichIngredientCrossRefByIdIngredient(id);
            return null;
        }
    }


    public LiveData<List<Salade>> getLesSalades(){
        return lesSalades;
    }

    public LiveData<List<Boisson>> getLesBoissons() {
        return lesBoissons;
    }

    public LiveData<List<Dessert>> getLesDesserts() {
        return lesDesserts;
    }

    public static class AsyncTaskTwoParams{
        private Sandwich sand;
        private Ingredient ingr;

        AsyncTaskTwoParams(Sandwich sandwich, Ingredient ingredient){
            this.sand=sandwich;
            this.ingr=ingredient;
        }
    }

    public void insertSandwichWithIngredients(Sandwich sandwich, Ingredient ingredient){
        new insertSandwichWithIngredientsAsyncTask(foodTruckDAO).execute(new AsyncTaskTwoParams(sandwich, ingredient));
    }

    public static class insertSandwichWithIngredientsAsyncTask extends AsyncTask<AsyncTaskTwoParams,Void,Void>{

        private FoodTruckDAO foodTruckDAO;

        public insertSandwichWithIngredientsAsyncTask(FoodTruckDAO foodTruckDAO){
            this.foodTruckDAO=foodTruckDAO;
        }

        @Override
        protected Void doInBackground(AsyncTaskTwoParams... asyncTaskTwoParams){
            foodTruckDAO.insertSandWithIngr(new SandwichIngredients(asyncTaskTwoParams[0].sand.getSandwichId(), asyncTaskTwoParams[0].ingr.getIngredientId()));
            return null;
        }

    }

    public void insertIngredientWithSandwichs(Sandwich sandwich, Ingredient ingredient){
        new insertIngredientWithSandwichsAsyncTask(foodTruckDAO).execute(new AsyncTaskTwoParams(sandwich, ingredient));
    }

    public static class insertIngredientWithSandwichsAsyncTask extends AsyncTask<AsyncTaskTwoParams,Void,Void>{

        private FoodTruckDAO foodTruckDAO;

        public insertIngredientWithSandwichsAsyncTask(FoodTruckDAO foodTruckDAO){
            this.foodTruckDAO=foodTruckDAO;
        }

        @Override
        protected Void doInBackground(AsyncTaskTwoParams... asyncTaskTwoParams){
            foodTruckDAO.insertIngredientWithSandwichs(new SandwichIngredients(asyncTaskTwoParams[0].sand.getSandwichId(), asyncTaskTwoParams[0].ingr.getIngredientId()));
            return null;
        }

    }



    public long insertSandwich(Sandwich s){
        try {
            return new insertAsyncTask(foodTruckDAO, s).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private static class insertAsyncTask extends AsyncTask<Void, Void, Long>{
        private FoodTruckDAO foodTruckDAO;
        private Sandwich s;
        insertAsyncTask(FoodTruckDAO fd, Sandwich s){
            foodTruckDAO=fd;
            this.s=s;
        }

        @Override
        protected Long doInBackground(Void... v){
            return foodTruckDAO.insertSand(s);

        }
    }

    public long insertIngredient(Ingredient ing){
        try {
            return new insertIngredientAsyncTask(foodTruckDAO, ing).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private static class insertIngredientAsyncTask extends AsyncTask<Void, Void, Long>{
        private FoodTruckDAO foodTruckDAO;
        private Ingredient ing;

        insertIngredientAsyncTask(FoodTruckDAO fd, Ingredient ing){
            foodTruckDAO=fd;
            this.ing=ing;
        }

        @Override
        protected Long doInBackground(Void... v){
            return foodTruckDAO.insertIngr(ing);

        }
    }

    public void deleteIngredients(){
        new deleteIngredientsAsyncTask(foodTruckDAO).execute();
    }

    private static class deleteIngredientsAsyncTask extends AsyncTask<Void, Void, Void>{
        private FoodTruckDAO foodTruckDAO;

        deleteIngredientsAsyncTask(FoodTruckDAO fd){
            foodTruckDAO=fd;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            foodTruckDAO.deleteIngredients();
            return null;
        }
    }

}
