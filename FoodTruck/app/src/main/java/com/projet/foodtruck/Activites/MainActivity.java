package com.projet.foodtruck.Activites;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.projet.foodtruck.R;

public class MainActivity extends AppCompatActivity {

    private ImageView parcourir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.parcourir=findViewById(R.id.recherche);

    }


    public void parcourir_categ(View view) {
        Intent intent = new Intent(getApplicationContext(), FoodCategories.class);
        startActivity(intent);
        finish();
    }

    public void gestion(View view) {
        Intent intent = new Intent(getApplicationContext(), AccueilGestion.class);
        startActivity(intent);
        finish();
    }


}