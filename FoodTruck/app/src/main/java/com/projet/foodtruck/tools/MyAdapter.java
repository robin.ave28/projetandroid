package com.projet.foodtruck.tools;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.projet.foodtruck.R;
import com.projet.foodtruck.models.Aliment;
import com.projet.foodtruck.models.Ingredient;
import com.projet.foodtruck.models.Sandwich;
import com.projet.foodtruck.models.SandwichWithIngredients;


import java.util.List;
import java.util.concurrent.ExecutionException;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private List<SandwichWithIngredients> lesSandwichs;
    private static InterfaceMyListener myListener;



    public MyAdapter(List<SandwichWithIngredients> sandwichs){ this.lesSandwichs=sandwichs;}


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener{

        private TextView tvNom;
        private TextView tvPrice;
        private TextView tvIngredients;
        private Sandwich sandwich;

        public MyViewHolder(@NonNull View itemView){
            super(itemView);
            tvNom=itemView.findViewById(R.id.nomSandwich);
            tvPrice=itemView.findViewById(R.id.priceSandwich);
            tvIngredients=itemView.findViewById(R.id.ingredientsSand);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);


        }

        public void display(SandwichWithIngredients sandwich){
            tvNom.setText(sandwich.sandwich.getName());

            tvPrice.setText(sandwich.sandwich.getPrice());
            String res = "Ingrédients : ";
            for(Ingredient i:sandwich.ingredients){
                res+=i.getName()+", ";
            }
            tvIngredients.setText(res);
            this.sandwich = sandwich.sandwich;
        }

        @Override
        public void onClick(View view) {
            try {
                myListener.onItemClick(getAdapterPosition(), view);
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        @Override
        public boolean onLongClick(View view) {
            System.out.println("TGGG");

            try {
                System.out.println("AIIEEEE");
                myListener.onItemLongClick(getAdapterPosition(), view);
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            return false;
        }
    }


    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sandwich,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        SandwichWithIngredients sandwich = lesSandwichs.get(position);
        holder.display(sandwich);
    }

    @Override
    public int getItemCount() {
        return lesSandwichs.size();
    }

    public static void setMyListener(InterfaceMyListener myListener) {
        MyAdapter.myListener = myListener;
    }
}

