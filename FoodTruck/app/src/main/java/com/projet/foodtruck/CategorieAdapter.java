//package com.projet.foodtruck;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.projet.foodtruck.models.Categorie;
//
//import java.util.List;
//
//public class CategorieAdapter extends BaseAdapter {
//
//    private Context context;
//    private List<Categorie> categorieList;
//    private LayoutInflater inflater;
//
//    public CategorieAdapter(Context context, List<Categorie> categorieList){
//        this.context=context;
//        this.categorieList=categorieList;
//        this.inflater =  LayoutInflater.from(context);
//    }
//
//    @Override
//    public int getCount() {
//        return categorieList.size();
//    }
//
//    @Override
//    public Categorie getItem(int i) {
//        return categorieList.get(i);
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return 0;
//    }
//
//    @Override
//    public View getView(int i, View view, ViewGroup viewGroup) {
//        view = inflater.inflate(R.layout.adapter_categorie, null);
//
//        // récupère les informations à propos de la categorie
//        Categorie currentCateg = getItem(i);
//        String categName = currentCateg.getCateg_name();
//        String mnemonic = currentCateg.getId();
//
//        TextView categNameView = view.findViewById(R.id.categ_name);
//        categNameView.setText(categName);
//
//        ImageView categIconView = view.findViewById(R.id.categ_icon);
//        String ressourceName = "categ_"+mnemonic;
//        int ressourceId = context.getResources().getIdentifier(ressourceName, "drawable", context.getPackageName());
//        categIconView.setImageResource(ressourceId);
//
//        return view;
//    }
//}
