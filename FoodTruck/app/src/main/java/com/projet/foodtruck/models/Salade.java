package com.projet.foodtruck.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.List;
import java.util.zip.ZipInputStream;

@Entity(tableName = "salades")
public class Salade {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;

    private List<String> ingredients;



    public Salade(String name, List<String> ingredients) {
        this.name = name;
        this.ingredients = ingredients;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return "Salade{" +
                "id=" + id +
                ", nom='" + name + '\'' +
                ", ingrédients=[" + ingredients +"]" +
                "}";
    }

}
