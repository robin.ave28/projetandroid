package com.projet.foodtruck.Activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projet.foodtruck.MyAdapterIngredient;
import com.projet.foodtruck.R;
import com.projet.foodtruck.models.FoodTruckViewModel;
import com.projet.foodtruck.models.Ingredient;
import com.projet.foodtruck.models.IngredientWithSandwichs;
import com.projet.foodtruck.models.Sandwich;
import com.projet.foodtruck.tools.InterfaceMyListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class AjoutIngredientSandwich extends AppCompatActivity {

    private List<Ingredient> lesIngredients = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MyAdapterIngredient myAdapter;
    private FoodTruckViewModel foodTruckViewModel;
    private List<Ingredient> ingredientsSelect = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajout_ingredient_sandwich);
        System.out.println("AHAHAHHAHAHAHAHAHHA");

        RecyclerView recyclerView = findViewById(R.id.recyclerIngredientToSand);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        myAdapter = new MyAdapterIngredient(lesIngredients);
        recyclerView.setAdapter(myAdapter);

        foodTruckViewModel = new ViewModelProvider(this).get(FoodTruckViewModel.class);

        foodTruckViewModel.getLesIngredients().observe(this, new Observer<List<IngredientWithSandwichs>>() {
            @Override
            public void onChanged(List<IngredientWithSandwichs> ingredients) {
                lesIngredients.clear();

                for(IngredientWithSandwichs i : ingredients){
                    System.out.println("Id d'ingredient -->");
                    System.out.println(i.ingredient.getIngredientId());

                    lesIngredients.add(i.ingredient);

                }
                myAdapter.notifyDataSetChanged();
                System.out.println(lesIngredients);
            }
        });

        MyAdapterIngredient.setMyListener(new InterfaceMyListener() {
            @Override
            public void onItemLongClick(int position, View view) {

            }

            @Override
            public void onItemClick(int position, View view) throws ExecutionException, InterruptedException {

                System.out.println("INGREDIENT trop cool");

                if(!ingredientsSelect.contains(lesIngredients.get(position))){
                    System.out.println("Oui");
                    ingredientsSelect.add(lesIngredients.get(position));

                }

                System.out.println(ingredientsSelect);

                //System.out.println(lesIngredients.get(position).getIngredientId());
                //System.out.println(ingredientsSelect.get(0).getIngredientId());

                //intent.putExtra("id", lesIngredients.get(position).getIngredientId());

                //setResult(Activity.RESULT_OK, intent);


            }
        });


    }



    public void envoyerIngr(View view) {

        Bundle bundle = new Bundle();

        bundle.putParcelableArrayList("ingredients", (ArrayList<? extends Parcelable>) ingredientsSelect);
        Intent intent = new Intent(getApplicationContext(), AjoutSandwich.class);
        intent.putExtras(bundle);
        //intent.putExtra("ingredients", ingredientsSelect.get(0));
        //intent.putParcelableArrayListExtra("ingredients", (ArrayList<? extends Parcelable>) ingredientsSelect);
        startActivity(intent);
        finish();

    }

    public void allerGestion(View view) {
        Intent intent = new Intent(getApplicationContext(), AjoutSandwich.class);
        startActivity(intent);
        finish();
    }
}
