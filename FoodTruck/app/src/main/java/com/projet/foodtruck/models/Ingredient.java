package com.projet.foodtruck.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "ingredients", indices = {@Index(value = {"name"}, unique = true)})
public class Ingredient implements Parcelable{

    @PrimaryKey(autoGenerate = true)
    private int ingredientId;

    private String name;

    public Ingredient(String name){
        this.name=name;
    }

    protected Ingredient(Parcel in){
        ingredientId=in.readInt();
        name = in.readString();
    }

    public static final Creator<Ingredient> CREATOR = new Creator<Ingredient>() {
        @Override
        public Ingredient createFromParcel(Parcel in) {
            return new Ingredient(in);
        }

        @Override
        public Ingredient[] newArray(int size) {
            return new Ingredient[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ingredientId);
        dest.writeString(name);

    }


    public int getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(int id) {
        this.ingredientId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "id=" + ingredientId +
                ", nom='" + name + '\'' +
                "}";
    }
}
