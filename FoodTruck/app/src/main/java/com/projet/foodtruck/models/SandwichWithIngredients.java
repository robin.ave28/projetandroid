package com.projet.foodtruck.models;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class SandwichWithIngredients {
    @Embedded public Sandwich sandwich;
    @Relation(
            parentColumn = "sandwichId",
            entityColumn = "ingredientId",
            associateBy = @Junction(SandwichIngredients.class)
    )
    public List<Ingredient> ingredients;
}
