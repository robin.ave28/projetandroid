package com.projet.foodtruck.DAO;


import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.projet.foodtruck.models.Boisson;
import com.projet.foodtruck.models.Dessert;
import com.projet.foodtruck.models.Ingredient;
import com.projet.foodtruck.models.IngredientWithSandwichs;
import com.projet.foodtruck.models.Salade;
import com.projet.foodtruck.models.Sandwich;
import com.projet.foodtruck.models.SandwichIngredients;
import com.projet.foodtruck.models.SandwichWithIngredients;

import java.util.List;

@Dao
public interface FoodTruckDAO {


    //Les INSERTS
    @Transaction
    @Insert(onConflict = REPLACE)
    public void insertSalade(Salade s);


    @Insert(onConflict = REPLACE)
    public void insertSandwich(Sandwich s);

    @Transaction
    @Insert(onConflict = REPLACE)
    public void insertBoisson(Boisson b);

    @Transaction
    @Insert(onConflict = REPLACE)
    public void insertDessert(Dessert d);

    @Transaction
    @Insert(onConflict = REPLACE)
    public void insertIngredient(Ingredient d);

    @Insert(onConflict = REPLACE)
    public long insertSand(Sandwich sand);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public long insertIngr(Ingredient ingr);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertSandWithIngr(SandwichIngredients sandwichIngredients);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertIngredientWithSandwichs(SandwichIngredients sandwichIngredients);



    //Les DELETES
    @Delete
    public void deleteSalade(Salade s);

    @Delete
    public void deleteSandwich(Sandwich s);

    @Delete
    public void deleteBoisson(Boisson b);

    @Delete
    public void deleteDessert(Dessert d);

    @Delete
    public void deleteIngredient(Ingredient in);

    //Les SELECT *
    @Query("SELECT * FROM SALADES")
    public LiveData<List<Salade>> toutesLesSalades();

    @Query("SELECT * FROM sandwichs")
    public LiveData<List<SandwichWithIngredients>> tousLesSandwichs();

    @Query("SELECT * FROM INGREDIENTS")
    public LiveData<List<IngredientWithSandwichs>> tousLesIngredients();

    @Query("SELECT * FROM ingredients where ingredientId=:id")
    public Ingredient getIngredient(Integer id);

    @Query("SELECT name FROM ingredients where name=:name")
    public String getIngredientName(String name);

    @Query("SELECT * FROM sandwichs where sandwichId=:id")
    public Sandwich getSandwich(Integer id);

    @Query("SELECT name FROM sandwichs where name=:name")
    public String getSandwichName(String name);

    @Query("SELECT * FROM sandwichs where sandwichId=:id")
    public SandwichWithIngredients getSandwichWithIngredients(Integer id);

    @Query("SELECT * FROM ingredients where ingredientId=:id")
    public IngredientWithSandwichs getIngredientWithSandwichs(Integer id);

    @Query("SELECT * FROM sandwichs natural join sandwichingredients where ingredientId=:id")
    public LiveData<List<Sandwich>> getSandwichsOfIngredient(Integer id);

//    @Query("SELECT * FROM sandwichs where id=:id")
//    public LiveData<Sandwich> getSandwich(Integer id);

    @Query("SELECT * FROM BOISSONS")
    public LiveData<List<Boisson>> toutesLesBoissons();

    @Query("SELECT * FROM DESSERTS")
    public LiveData<List<Dessert>> tousLesDesserts();



    //Les SELECT de quantité
    @Query("Select count(*) from SANDWICHS")
    public LiveData<Integer> nbSandwichs();

    @Query("Select count(*) from INGREDIENTS")
    public LiveData<Integer> nbIngredients();

    //Les DELETES
    @Query("Delete from SANDWICHS ")
    public void deleteSandwichs();

    @Query("Delete from SANDWICHS where sandwichId=:id")
    public void deleteSandwich(Integer id);

    @Query("Delete from INGREDIENTS where ingredientId=:id")
    public void deleteIngredient(Integer id);

    @Query("Delete from sandwichingredients ")
    public void deleteSandwichIngredientCrossRef();

    @Query("Delete from sandwichingredients where sandwichId=:id ")
    public void deleteSandwichIngredientCrossRefById(Integer id);

    @Query("Delete from sandwichingredients where ingredientId=:id ")
    public void deleteSandwichIngredientCrossRefByIdIngredient(Integer id);

    @Query("Delete from ingredients")
    public void deleteIngredients();

    @Transaction
    @Query("SELECT * FROM sandwichs")
    public List<SandwichWithIngredients> getSandwichWithIngredients();

    @Transaction
    @Query("SELECT * FROM ingredients")
    public List<IngredientWithSandwichs> getIngredientWithSandwich();



}
