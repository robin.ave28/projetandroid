package com.projet.foodtruck.Activites;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projet.foodtruck.R;
import com.projet.foodtruck.models.FoodTruckViewModel;
import com.projet.foodtruck.models.IngredientWithSandwichs;
import com.projet.foodtruck.models.Sandwich;
import com.projet.foodtruck.models.SandwichWithIngredients;
import com.projet.foodtruck.tools.MyAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class SandwichsByIngredient  extends AppCompatActivity {

    private FoodTruckViewModel foodTruckViewModel;
    private IngredientWithSandwichs ingredientWithSandwichs;
    private List<SandwichWithIngredients> lesSandwichs = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sandwichs_ingredient);


        foodTruckViewModel = new ViewModelProvider(this).get(FoodTruckViewModel.class);

        RecyclerView recyclerView = findViewById(R.id.recyclerSandwich);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

//        for(Sandwich s:ingredientWithSandwichs.sandwichs){
//            int id = s.getSandwichId();
//            try {
//                SandwichWithIngredients sand = foodTruckViewModel.getSandwichWithIngredients(id);
//                lesSandwichs.add(sand);
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
        myAdapter = new MyAdapter(lesSandwichs);
        recyclerView.setAdapter(myAdapter);

        Bundle bundle = getIntent().getExtras();
        int id = (int) bundle.get("id");

        try {
            ingredientWithSandwichs = foodTruckViewModel.getIngredientWithSandwichs(id);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        foodTruckViewModel.getSandwichsOfIngredient(ingredientWithSandwichs.ingredient.getIngredientId()).observe(this, new Observer<List<Sandwich>>() {
            @Override
            public void onChanged(List<Sandwich> sandwichs) {
                lesSandwichs.clear();
                for(Sandwich s:sandwichs){
                    try {
                        lesSandwichs.add(foodTruckViewModel.getSandwichWithIngredients(s.getSandwichId()));
                        myAdapter.notifyDataSetChanged();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void retourGestionIngredient(View view) {
        Intent intent = new Intent(getApplicationContext(), GestionIngredient.class);
        startActivity(intent);
        finish();
    }
}
