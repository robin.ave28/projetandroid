package com.projet.foodtruck.Activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.projet.foodtruck.R;
import com.projet.foodtruck.models.FoodTruckViewModel;
import com.projet.foodtruck.models.Ingredient;

import java.util.concurrent.ExecutionException;

public class AjoutIngredient extends AppCompatActivity {

    private FoodTruckViewModel foodTruckViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajout_ingredient);

        foodTruckViewModel = new ViewModelProvider(this).get(FoodTruckViewModel.class);

    }

    @Override
    public void finish() {
        super.finish();
    }


    public void backToGestionIngr(View view) {
        Intent intent = new Intent(this,GestionIngredient.class);
        //mStartForResult.launch(intent);
        startActivity(intent);
        finish();
    }

    public void addIngredient(View view) throws ExecutionException, InterruptedException {
        TextView tvName = findViewById(R.id.editIngredient);
        String nom = tvName.getText().toString();
        if(foodTruckViewModel.getIngredientName(nom)==null){
            Ingredient ingr = new Ingredient(nom);
            System.out.println("Id ingrédient");
            System.out.println(ingr.getIngredientId());
            System.out.println(nom);
            long idIngr = foodTruckViewModel.insertIngredient(ingr);
            int idIngredient = (int) idIngr;
            ingr = foodTruckViewModel.getIngredient(idIngredient);
            Intent intent = new Intent();
            intent.putExtra("ingredient", ingr);
            setResult(Activity.RESULT_OK, intent);
        }else{
            Toast toast = Toast.makeText(getApplicationContext(), "L'ingrédient existe déjà !", 2);
            toast.show();
        }

        finish();
    }
}
