package com.projet.foodtruck.DAO;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.projet.foodtruck.models.Salade;

import java.util.List;

@Dao
public interface SaladeDAO {

    @Insert
    public void insert(Salade s);

    @Query("SELECT * FROM SALADES")
    List<Salade> toutesLesSalades();

    @Query("SELECT count(*) FROM SALADES")
    int nbSalades();
}
