package com.projet.foodtruck.Activites;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;


import androidx.appcompat.app.AppCompatActivity;


import com.projet.foodtruck.R;

public class FoodCategories extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_categories);

    }


    public void allerSandwich(View view) {
        Intent intent = new Intent(getApplicationContext(), LookSandwich.class);
        startActivity(intent);
        finish();
    }


    public void allerAccueil(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }
}
