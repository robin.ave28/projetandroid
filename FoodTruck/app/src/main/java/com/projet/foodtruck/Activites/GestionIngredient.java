package com.projet.foodtruck.Activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projet.foodtruck.MyAdapterIngredient;
import com.projet.foodtruck.R;
import com.projet.foodtruck.models.FoodTruckViewModel;
import com.projet.foodtruck.models.Ingredient;
import com.projet.foodtruck.models.IngredientWithSandwichs;
import com.projet.foodtruck.models.Sandwich;
import com.projet.foodtruck.tools.InterfaceMyListener;
import com.projet.foodtruck.tools.MyAdapter;

import java.util.ArrayList;
import java.util.List;

public class GestionIngredient extends AppCompatActivity {

    private FoodTruckViewModel foodTruckViewModel;
    private List<Ingredient> lesIngredients = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MyAdapterIngredient myAdapter;


    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {

                    if (result.getResultCode() == Activity.RESULT_OK) {
                        System.out.println("INGREDIENT :");
                        Intent intent = result.getData();
                        Ingredient ingr = intent.getParcelableExtra("ingredient");
                        System.out.println(ingr);
                        lesIngredients.add(ingr);
                        //foodTruckViewModel.insertIngredient(ingr);
                        myAdapter.notifyItemChanged(lesIngredients.size()-1);

                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gestion_ingredient);
        System.out.println("OUIIII");

        foodTruckViewModel = new ViewModelProvider(this).get(FoodTruckViewModel.class);
        foodTruckViewModel.getNbIngredients().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                TextView tv = findViewById(R.id.nbIngredients);
                tv.setText("Nb ingredients : "+integer);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recyclerIngredient);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        myAdapter = new MyAdapterIngredient(lesIngredients);
        recyclerView.setAdapter(myAdapter);

        foodTruckViewModel.getLesIngredients().observe(this, new Observer<List<IngredientWithSandwichs>>() {
            @Override
            public void onChanged(List<IngredientWithSandwichs> ingredients) {
                lesIngredients.clear();
                System.out.println("ON RENTRE DANS ONCHANGED");
                System.out.println(ingredients);
                for(IngredientWithSandwichs i : ingredients){
                    lesIngredients.add(i.ingredient);

                }
                myAdapter.notifyDataSetChanged();
                System.out.println(lesIngredients);
            }
        });

        MyAdapterIngredient.setMyListener(new InterfaceMyListener() {
            @Override
            public void onItemLongClick(int position, View view) {
                System.out.println("ALLLLEDDD");
                Integer id = lesIngredients.get(position).getIngredientId();
                lesIngredients.remove(position);
                foodTruckViewModel.deleteIngredient(id);
                foodTruckViewModel.deleteSandwichsIngredientCrossRefByIdIngredient(id);
                myAdapter.notifyItemRemoved(position);
            }

            @Override
            public void onItemClick(int position, View view) {
                Intent intent = new Intent(getApplicationContext(), SandwichsByIngredient.class);
                intent.putExtra("id", lesIngredients.get(position).getIngredientId());
                startActivity(intent);
                finish();

            }
        });

    }



    public void genererIngredient(View view) {
        for(int i=0;i<10;i++){
            System.out.println("Ajout ingredient");
            Ingredient ingr = new Ingredient("Ingredient"+i);
            foodTruckViewModel.insertIngredient(ingr);
        }

    }

    public void supprimerIngredient(View view) {
        foodTruckViewModel.deleteIngredients();
        lesIngredients.clear();
        myAdapter.notifyDataSetChanged();
    }

    public void allerGestion(View view) {
        Intent intent = new Intent(getApplicationContext(), AccueilGestion.class);
        startActivity(intent);
        finish();
    }

    public void ajoutIngredient(View view) {
        Intent intent = new Intent(this,AjoutIngredient.class);
        mStartForResult.launch(intent);
        //startActivity(intent);
        //finish();
    }

}
