package com.projet.foodtruck.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.Insert;
import androidx.room.PrimaryKey;

import com.projet.foodtruck.DAO.ListTypeConverter;

import java.util.List;

@Entity(tableName = "sandwichs", indices = {@Index(value = {"name"}, unique = true)})
public class Sandwich implements Parcelable {



    @PrimaryKey(autoGenerate = true)
    private int sandwichId;
    public String name;
    private String image = "categ_sandwich";
    public String price;


    public Sandwich(String name, String price, String image) {
        this.name = name;
        this.price = price;
    }

    protected Sandwich(Parcel in) {
        sandwichId=in.readInt();
        name = in.readString();
        price = in.readString();
    }

    public static final Creator<Sandwich> CREATOR = new Creator<Sandwich>() {
        @Override
        public Sandwich createFromParcel(Parcel in) {
            return new Sandwich(in);
        }

        @Override
        public Sandwich[] newArray(int size) {
            return new Sandwich[size];
        }
    };

    public int getSandwichId() {
        return this.sandwichId;
    }

    public void setSandwichId(int id) {
        this.sandwichId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String ingredients) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(sandwichId);
        dest.writeString(name);
        dest.writeString(price);

    }

    @Override
    public String toString() {
        return "Sandwich{" +
                "id=" + sandwichId +
                ", nom='" + name + '\'' +
                ", price=" + price  +
                "}";
    }
}
